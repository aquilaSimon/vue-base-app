export default {
  PROJECTS_TITLE: 'Projets',
  PROJECTS_EVENT: 'projects_event',
  CARDS_TITLE: 'Cartes',
  CARDS_EVENT: 'card_event',
};
