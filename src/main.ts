import { createApp } from 'vue';
import { createI18n } from 'vue-i18n';
import vuetify from './plugins/vuetify';
import App from './App.vue';
import router from './router';
import store from './store';
import './assets/tailwind.css';
import lang from './lang';

const i18n = createI18n({
  locale: navigator.language,
  messages: lang,
  fallbackLocale: 'en',
});

createApp(App)
  .use(store)
  .use(router)
  .use(vuetify)
  .use(i18n)
  .mount('#app');
