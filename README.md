# timeforecast_vue

## Project setup
```
yarn 
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### use store with composition api
```
https://dev.to/shubhadip/vue-3-vuex-4-modules-typescript-2i2o
```

### change local language with composition api
```
import { useI18n } from 'vue-i18n';
export default defineComponent({
  setup() {
    const { locale } = useI18n({ useScope: 'global' });
    locale.value = 'fr';
  }
})
```

