module.exports = {
  css: {
    loaderOptions: {
      scss: {
        additionalData: `@import "@/assets/styles/variables/_shared.scss";`,
      },
    },
  },

  transpileDependencies: [
    'vuetify',
  ],
};
